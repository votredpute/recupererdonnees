var http = require("http");
var mongo = require("mongodb");
var request = require('request');
var Sync = require("sync");
var xml2js = require('xml2js');



// --------------- Variables globales

const DATE_MAX_PROJETSLOI = '2016-01-01';
// Objet JavaScript représentant les dépenses des députés.
var DEPENSES_DEPUTES_JS = null;
const MLAB_URI = 'mongodb://votredepute:inf6150@ds139899.mlab.com:39899/heroku_fpw34v7h';
const MLAB_BD_DEPUTES = 'deputes';
const MLAB_BD_PROJETSLOI = 'projetsloi';



// --------------- Requêtes

/**
 * Requête de base pour interroger openparliament.ca.
 * Il sera nécessaire de régler l'attribut "uri".
 * À instancier avec le mot-clé "new".
 */
function REQ_BASE_OPENPARLIAMENT() {
    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca';
    this.uri = '';
    this.qs = {};
    this.json = true;
}

/**
 * Requête pour obtenir la liste des dépenses des députés à partir du site officiel du parlement.
 * Les données retournées sont au format XML.
 */
function REQ_DEPENSES_DEPUTES() {
    this.method = 'GET';
    this.baseUrl = 'http://www.parl.gc.ca/PublicDisclosure/MemberExpenditures.aspx',
    this.uri = '';
    this.qs = {
        FormatType: 'XML',
        ID: 'MER2017Q2D',
        Language: 'F'
    };
    // Le résulat obtenu est au format XML.
    this.json = false;
}

/**
 * Requête pour obtenir la fiche complète d'un politicien à partir de openparliament.ca.
 * 
 * @param aNomComplet
 * Le nom complet du politicien pour lequel obtenir la fiche.
 * Notez bien : aucun accent ("Gérard" doit être passé comme "Gerard"), un seul espace en blanc
 * pour séparer le prénom et le nom.
 */
function REQ_FICHE_POLITICIEN(aNomComplet) {
    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca/politicians';
    this.uri = aNomComplet.replace(" ", "-").toLowerCase();
    this.qs = {};
    this.json = true;
}

/**
 * Requête pour obtenir la fiche complète d'un projet de loi à partir de openparliament.ca.
 * 
 * @param aNumeroProjetLoi
 * Le numéro donné au projet de loi.
 * 
 * @param aNumeroSession
 * Le numéro de la session à laquelle le projet de loi a été voté.
 * En effet, un projet de loi peut avoir été révisé de nombreuses fois pour tenter d'être adopté.
 */
function REQ_FICHE_PROJET_LOI(aNumeroProjetLoi, aNumeroSession) {
    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca/bills';
    this.uri = aNumeroSession + "/" + aNumeroProjetLoi;
    this.qs = {};
    this.json = true;
}

/**
 * Requête pour obtenir la liste des politiciens actifs à partir de openparliament.ca.
 * Par défaut, le retour d'information est divisé en "page" de 200 politiciens.
 * À instancier avec le mot-clé "new".
 */
function REQ_LISTE_POLITICIENS_ACTIFS() {
    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca';
    this.uri = 'politicians';
    this.qs = {
        // La chambre des communes est composée de 338 députés.
        // Le résultat sera donc réparti sur deux pages.
        limit: 200
    };
    this.json = true;
}

/**
 * Requête pour obtenir la liste des projets de loi à partir de openparliament.ca.
 * Par défaut, le retour d'information est divisé en "page" de 200 projets de loi.
 * À instancier avec le mot-clé "new".
 * 
 * @param aDateMax
 * Limiter le résultat aux projets de loi soumis >= cette date. Si null, tous les projets de loi
 * seront retournés.
 * Notez bien : la date fournie doit être au format international : aaaa-mm-jj.
 */
function REQ_LISTE_PROJETS_LOI(aDateMax) {
    var timestamp = Date.parse(aDateMax);
    var dateValide = !(isNaN(timestamp));

    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca';
    this.uri = 'bills';
    this.qs = { limit: 200 };
    if (dateValide) this.qs.introduced__gt = aDateMax;
    this.json = true;
}

/**
 * Pour un politicien donné, requête pour obtenir la liste de ses votes sur les différents projet de loi.
 * Par défaut, le retour d'information est divisé en "page" de 200 projets de loi.
 * À instancier avec le mot-clé "new".
 * 
 * @param aNomComplet
 * Le nom complet du politicien pour lequel obtenir les votes.
 * Notez bien : aucun accent ("Gérard" doit être passé comme "Gerard"), un seul espace en blanc
 * pour séparer le prénom et le nom.
 */
function REQ_LISTE_VOTES_DUN_POLITICIEN(aNomComplet) {
    this.method = 'GET';
    this.baseUrl = 'https://api.openparliament.ca';
    this.uri = 'votes/ballots';
    this.qs = { limit: 200 }
    this.qs.politician = aNomComplet.replace(" ", "-").toLowerCase();
    this.json = true;
}



// --------------- Fonctions

/**
 * Charge en mémoire les dépenses des politiciens.
 * Les dépenses sont assignées à la variable globale DEPENSES_DEPUTES_JS.
 * 
 * @param callback
 * Fonction de rappel à appeler pour signaler la fin de la du chargement des données.
 * Obligatoire, sans quoi la présente fonction resetera bloquée indéfiniement.
 * 
 * 
 * @post
 * Les dépenses sont chargées en mémoire dans la variable globale DEPENSES_DEPUTES_JS.
 * 
 * @see DEPENSES_DEPUTES_JS
 */
function charger_enmemoire_depenses(callback) {
    Sync(function() {
        DEPENSES_DEPUTES_JS = obtenirDepensesDeputes.sync(null);
        callback(null);
    });
}

/**
 * Retourne les dépenses d'un députéé bien précis.
 * Important : il est obligatoire de charger d'abord les dépenses des députés et d'assigner celles-ci
 * à la variable globale DEPENSES_DEPUTES_JS. Ceci est pour des raisons de performance.
 * 
 * @param aIdElectionsCanada
 * L'ID unique d'Élections Canada identifiant le politicien pour lequel obtenir les dépenses.
 * 
 * @return
 * Objet JavaScript représentant les dépenses du député.
 * null est retourné s'il est impossible de trouver les dépenses du politicien.
 * 
 * @see obtenirDepensesDeputes()
 * @see DEPENSES_DEPUTES_JS
 */
function obtenirDepensesDunDepute(aIdElectionsCanada) {
    var depensesDepute = null;
    var deputesCompte = (DEPENSES_DEPUTES_JS != null) ? DEPENSES_DEPUTES_JS.Report.length : 0;
    var depenses = (deputesCompte > 0) ? DEPENSES_DEPUTES_JS.Report : null;

    for (i = 0; i < deputesCompte && depensesDepute == null; ++i) {
        if (aIdElectionsCanada == depenses[i].Constituency.electionCanadaCode) {
            depensesDepute = depenses[i];
        }
    }

    return depensesDepute;
}

/**
 * Interroge le site du gouvernement parl.gc.ca pour obtenir la liste des dépenses des députés.
 * 
 * @param callback
 * Fonction de rappel à qui passer en argument le résultat (un objet JS).
 */
function obtenirDepensesDeputes(callback) {
    var parser = new xml2js.Parser({
                        explicitRoot: false,
                        rootName: 'MemberExpenditureReports',
                        explicitArray: false,
                        mergeAttrs: true
                    });
    var requete = new REQ_DEPENSES_DEPUTES();
    
    request
    (requete,
    // Fonction à exécuter suite à l'obtention du résultat de la requête
    function(err, res, depensesXML) {
        parser.parseString(depensesXML, function (err, depensesJS) {
            callback(null, depensesJS);
        });
    })
    .on('error', function(err) { console.log(err); });
}

/**
 * Interroge openparliament.ca pour obtenir une liste d'objets (une liste de politiciens par
 * exemple).
 * Les informations de chaque objet de la liste sont souvent très sommaires et il faudra
 * consulter le lien de l'attribut "url" pour en obtenir le détail complet.
 * 
 * @param aRequete
 * Objet JavaScript correspondant à une requête comprise par le module Node.js "request".
 * 
 * @param callback
 * Fonction de rappel à qui passer en argument le résultat (la liste des objets).
 * Le résultat sera un objet JavaScript du genre :
 * {
 *  { <Objet 1> },
 *  { <Objet 2> },
 *  etc... 
 * }
 * 
 * @see
 * REQ_LISTE_*
 */
function obtenirListeSommaire(aRequete, callback) {
    var objets = null;

    request
    (aRequete,
    // Fonction à exécuter suite à l'obtention du résultat de la requête.
    function(err, res, result) {
        objets = result.objects;

        // Les résultats ne tenaient pas sur une seule "page". On doit appeler la Fonction
        // récursivement pour obtenir le reste des résultats.
        if (result.pagination.next_url) {
            var objetsSuite = null;
            // On "clone" la requête originale, mais on demande la prochaine page des résultats.
            var requeteSuite =JSON.parse(JSON.stringify(aRequete));
            requeteSuite.uri = result.pagination.next_url;

            Sync(function() {
                objetsSuite = obtenirListeSommaire.sync(null, requeteSuite);
                callback(null, objets.concat(objetsSuite));
            });
        }
        // Fin de la récusivité.
        else {
            callback(null, objets);
        }
    })
    .on('error', function(err) { console.log(err); });
}

/**
 * Interroge openparliament.ca pour obtenir la fiche détaillée d'un objet (un politicien par
 * exemple).
 * 
 * @param aRequete
 * Objet JavaScript correspondant à une requête comprise par le module Node.js "request".
 * 
 * @param callback
 * Fonction de rappel à qui passer en argument le résultat (la fiche (l'objet) détaillée).
 * 
 * @see
 * REQ_FICHE_*
 */
function obtenirFicheDetaillee(aRequete, callback) {
    var unObjet = null;

    request
    (aRequete,
    // Fonction à exécuter suite à l'obtention du résultat de la requête
    function(err, res, result) {
        unObjet = result;
        callback(null, unObjet);
    })
    .on('error', function(err) { console.log(err); });
}

/**
 * Met à jour la liste des politiciens actifs dans notre base de données.
 */
function mettre_ajour_politiciens() {
    Sync(function() {
        var requete = new REQ_LISTE_POLITICIENS_ACTIFS();
        var politiciens = [];
        // Il est important de charger en mémoire les dépenses avant de débuter.
        charger_enmemoire_depenses.sync(null);
        var polsSommaire = obtenirListeSommaire.sync(null, requete);

        console.log('Mise à jour des politiciens en cours.');
        // On obtient la fiche complète de chaque politicien.
        for (var i in polsSommaire) {
            var unPoliticien = null;

            requete = new REQ_BASE_OPENPARLIAMENT();
            requete.uri = polsSommaire[i].url
            unPoliticien = obtenirFicheDetaillee.sync(null, requete);
            politiciens.push(unPoliticien);
            console.log('...politicien ' + (parseInt(i) + 1) + '/' + polsSommaire.length);
        }

        // On intègre à chaque politicien sa liste de dépenses.
        for (var i in politiciens) {
            var depensesDunPol = null;
            var idElectionCan = null;
            var unPoliticien = politiciens[i];

            if (typeof unPoliticien.memberships[0].riding.id != 'undefined') {
                idElectionCan = unPoliticien.memberships[0].riding.id;
            }
            depensesDunPol = obtenirDepensesDunDepute(idElectionCan);
            unPoliticien.expenditures = depensesDunPol;
        }

        mongo.MongoClient.connect(MLAB_URI, function(err, bd) {
            var deputesBd = bd.collection(MLAB_BD_DEPUTES);
            
            deputesBd.remove();
            deputesBd.insert(politiciens, function(err, result) {
                if (!err) {
                    console.log("Mise à jour des politiciens complétée avec succès.");
                } else {
                    console.log("Erreur lors de la mise à jour des politiciens.");
                    console.log(err);
                }
            })
        });
    });
}

/**
 * Met à jour la liste des projets de loi dans notre base de données.
 */
function mettre_ajour_projetsloi() {
    Sync(function() {
        var requete = new REQ_LISTE_PROJETS_LOI(DATE_MAX_PROJETSLOI);
        var lois = [];
        var uneLoi = null;
        var voteParParti = null;
        var voteParPoliticien = null;
        var loisSommaire = obtenirListeSommaire.sync(null, requete);

        console.log('Mise à jour des projets de lois en cours.');
        for (var i in loisSommaire) {
            // La fiche détaillée du projet de loi.
            requete = new REQ_BASE_OPENPARLIAMENT();
            requete.uri = loisSommaire[i].url
            uneLoi = obtenirFicheDetaillee.sync(null, requete);

            // On intègre à la fiche de loi le résultat, par parti, du dernier vote.
            voteParParti = null;
            if (uneLoi.vote_urls.length) {
                requete = new REQ_BASE_OPENPARLIAMENT();
                requete.uri = uneLoi.vote_urls[0];
                voteParParti = obtenirFicheDetaillee.sync(null, requete);
            }
            uneLoi.vote_parties = voteParParti;

            // On intègre à la fiche de loi le résultat, par politicien, du dernier vote.
            voteParPoliticien = null;
            if (voteParParti != null && typeof voteParParti.related.ballots_url != 'undefined') {
                requete = new REQ_BASE_OPENPARLIAMENT();
                requete.uri = voteParParti.related.ballots_url;
                voteParPoliticien = obtenirListeSommaire.sync(null, requete);
            }
            uneLoi.vote_politicians = voteParPoliticien;

            lois.push(uneLoi);
            console.log('...projet de loi ' + (parseInt(i) + 1) + '/' + loisSommaire.length);
        }

        mongo.MongoClient.connect(MLAB_URI, function(err, bd) {
            var loisBd = bd.collection(MLAB_BD_PROJETSLOI);
            
            loisBd.remove();
            loisBd.insert(lois, function(err, result) {
                if (!err) {
                    console.log("Mise à jour des projets de loi complétée avec succès.");
                } else {
                    console.log("Erreur lors de la mise à jour des projets de loi.");
                    console.log(err);
                }
            })
        });
    });
}



// --------------- Programme

mettre_ajour_projetsloi();
mettre_ajour_politiciens();



// --------------- Exemples pour mieux comprendre le code

/*
Sync(function() {
    // ----- Affichage des dépenses d'un politicien
    var depensesDepute = null;
    depensesDepute = obtenirDepensesDunDepute(35046);
    console.log("Obtention des dépenses du députe : " + depensesDepute.Member.firstName + ".");
    console.log("\n");
    
    // ----- Affichage de la fiche d'un politicien
    maRequete = new REQ_FICHE_POLITICIEN("Gerard Deltell");
    var unPoliticien = obtenirFicheDetaillee.sync(null, maRequete);
    console.log("Fiche du politicien obtenue.");
    console.log("Affiche des données sur le politicien.");
    console.log(unPoliticien);
    console.log("\n");

    // ----- Liste sommaire des politiciens
    maRequete = new REQ_LISTE_POLITICIENS_ACTIFS();
    var politiciens = obtenirListeSommaire.sync(null, maRequete);
    console.log("Liste des politiciens actifs obtenue.");
    console.log("Affichage du nom des politiciens.");
    for (i in politiciens) {
        console.log(politiciens[i].name);
    }
    console.log("\n");

    // ----- Liste sommaire des projets de loi
    var dateMax = "2016-06-01";
    maRequete = new REQ_LISTE_PROJETS_LOI(dateMax);
    var projetsLoi = obtenirListeSommaire.sync(null, maRequete);
    console.log("Liste des projets de loi obtenue.");
    console.log("Affichage des projets de loi.");
    for (i in projetsLoi) {
        console.log(projetsLoi[i].name.fr);
    }
    console.log("\n");

    // ----- Liste sommaire des votes d'un politicien
    var politicien = "Gerard Deltell";
    maRequete = new REQ_LISTE_VOTES_DUN_POLITICIEN(politicien);
    var votes = obtenirListeSommaire.sync(null, maRequete);
    console.log("Liste des votes de " + politicien + " obtenue.");
    console.log("Affichage des votes.");
    for (i in votes) {
        console.log(votes[i].ballot);
    }
    console.log("\n");

    console.log("Le programme est terminé.");
});
*/